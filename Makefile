##
## Makefile for eval_expr in /home/taieb_t/rendu/Piscine-C-eval_expr
## 
## Made by taieb_t$
## Login   <taieb_t@epitech.net>
## 
## Started on  Mon Oct 21 20:33:30 2013 taieb_t$
## Last update Sat Feb  8 16:25:53 2014 taieb_t$
##

RM	=	rm -f

LN	=	ln -f

SRC	=	malloc.c \
		tools_block.c \
		tools_memory.c

OBJS	=	$(SRC:.c=.o)

NAME	=	libmy_malloc_$(HOSTTYPE).so

LINK	=	libmy_malloc.so

CFLAGS	+=	-Wextra -Wall -Werror -fPIC -pedantic
CFLAGS	+=	-lpthread

all: $(LINK)

$(NAME): $(OBJS)
	gcc -shared -fPIC -o $(NAME) $(OBJS) $(LIB)

$(LINK): $(NAME)
	$(LN) $(NAME) $(LINK)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(LINK)

re: fclean all

.PHONY: all clean fclean re
