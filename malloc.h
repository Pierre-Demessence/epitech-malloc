/*
** malloc.h for  in /home/taieb_t/Desktop/rendu_malloc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Fri Feb  7 11:14:35 2014 taieb_t$
** Last update Sat Feb  8 16:30:23 2014 taieb_t$
*/

#ifndef		MALLOC_H_
# define	MALLOC_H_

# define _BSD_SOURCE
# include <sys/types.h>
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <pthread.h>

# define SIZE_BLOCK 40
# define ALIGN(x) (((((x) - 1) / 4) * 4) + 4)

struct			s_block
{
  size_t		size;
  struct s_block	*next;
  struct s_block	*prev;
  int			free;
  void			*ptr;
  char			data[1];
};
typedef struct s_block	*t_block;

/*
** malloc.c
*/
void		show_alloc_mem();
void		*malloc(size_t taille);
void		*calloc(size_t nelem, size_t elsize);
void		free(void *ptr);
void		*realloc(void *ptr, size_t taille);

/*
** tools_block.c
*/
t_block		get_block(void *p);
t_block		fusion(t_block b);
t_block		extend_heap(t_block last, size_t size);
void		split_block(t_block block, size_t size);
void		copy_block(t_block source, t_block dest);

/*
** tools_memory.c
*/
t_block		search_block(t_block *last, size_t size, void *base);
int		valid_addr(void *p, void *base);
void		*unlock_mutex(pthread_mutex_t	*mutex);

#endif /* !MALLOC_H_ */
