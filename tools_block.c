/*
** test.c for  in /home/taieb_t/Desktop/rendu_malloc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Fri Feb  7 11:47:59 2014 taieb_t$
** Last update Sat Feb  8 15:34:14 2014 taieb_t$
*/

#include "malloc.h"

t_block		get_block(void *p)
{
  char		*tmp;
  tmp = p;
  return (p = tmp -= SIZE_BLOCK);
}

t_block		fusion(t_block b)
{
  if (b->next && b->next->free)
    {
      b->size += SIZE_BLOCK + b->next->size;
      b->next = b->next->next;
      if (b->next)
	b->next->prev = b;
    }
  return (b);
}

t_block		extend_heap(t_block last,
			    size_t size)
{
  t_block	b;
  long		test;
  long		res;

  if ((b = sbrk(SIZE_BLOCK)) == (void *) -1)
    return (NULL);
  if ((int)size <= 0)
    return (NULL);
  res = (SIZE_BLOCK + size) % getpagesize();
  if (res != 0)
    size += getpagesize() - res;
  test = (long)sbrk(size);
  if (test < 0)
    return (NULL);
  b->size = size;
  b->next = NULL;
  b->prev = last;  b->ptr = b->data;
  if (last)
    last->next = b;
  b->free = 0;
  return (b);
}

void		split_block(t_block block, size_t size)
{
  t_block	new_block;

  new_block = (t_block)(block->data + size);
  new_block->size = block->size - size - SIZE_BLOCK;
  new_block->next = block->next;
  new_block->prev = block;
  new_block->free = 1;
  new_block->ptr = new_block->data;
  block->size = size;
  block->next = new_block;
  if (new_block->next)
    new_block->next->prev = new_block;
}

void		copy_block(t_block source, t_block dest)
{
  int		*data1;
  int		*data2;

  data1 = source->ptr;
  data2 = dest->ptr;
  memcpy(data2, data1, (source->size > dest->size ?
			dest->size : source->size));
}
