/*
** malloc.c for  in /home/taieb_t/Desktop/malloc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Wed Feb  5 12:52:10 2014 taieb_t$
** Last update Sat Feb  8 13:55:22 2014 taieb_t$
*/

#include "malloc.h"

static void		*base = NULL;
static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;

void		*malloc(size_t taille)
{
  t_block	b;
  t_block	last;
  size_t	size;

  pthread_mutex_lock(&mutex);
  size = ALIGN(taille);
  if (base && (last = base))
    {
      if ((b = search_block(&last, size, base)))
	{
	  if ((b->size - size) >= SIZE_BLOCK + 4)
	    split_block(b, size);
	  b->free = 0;
	}
      else if (!(b = extend_heap(last, size)))
	return (unlock_mutex(&mutex));
    }
  else
    {
      if (!(b = extend_heap(NULL, size)))
	return (unlock_mutex(&mutex));
      base = b;
    }
  pthread_mutex_unlock(&mutex);
  return (b->data);
}

void		*calloc(size_t nelem, size_t elsize)
{
  size_t	*new_size;
  size_t	s_align;

  new_size = malloc(nelem * elsize);
  if (new_size)
    {
      s_align = ALIGN(nelem * elsize);
      memset(new_size, 0, s_align);
    }
  return (new_size);
}

void		show_alloc_mem()
{
  t_block	b;

  b = (t_block)base;
  printf("\n**************\n");
  printf("SHOW ALLOC MEM\n");
  printf("**************\n\n");
  while (b)
    {
      if (!b->free)
	printf("%p - %p : %ld octets\n",
	       (void *)&(b->data),
	       (void *)(&(b->data) + b->size), b->size);
      b = b->next;
    }
}

void		free(void *ptr)
{
  t_block	b;

  pthread_mutex_lock(&mutex);
  if (valid_addr(ptr, base))
    {
      b = get_block(ptr);
      b->free = 1;
      if (b->prev && b->prev->free)
	b = fusion(b->prev);
      if (b->next)
	fusion(b);
      else
	{
	  if (b->prev)
	    b->prev->next = NULL;
	  else
	    base = NULL;
	  brk(b);
	}
    }
  pthread_mutex_unlock(&mutex);
}

void		*realloc(void *ptr, size_t taille)
{
  size_t	size;
  t_block	b;
  t_block	new_block;
  void		*new_ptr;

  if (!ptr)
    return (malloc(taille));
  size = ALIGN(taille);
  b = get_block(ptr);
  if (b->size >= size)
    {
      if (b->size - size >= (SIZE_BLOCK + 4))
  	split_block(b, size);
    }
  else
    {
      if (!(new_ptr = malloc(size)))
	return (NULL);
      new_block = get_block(new_ptr);
      copy_block(b, new_block);
      free(ptr);
      return (new_ptr);
    }
  return (ptr);
}
