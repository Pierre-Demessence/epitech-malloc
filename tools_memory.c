/*
** tools_memory.c for  in /home/taieb_t/Desktop/rendu_malloc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Sat Feb  8 13:28:53 2014 taieb_t$
** Last update Sat Feb  8 13:51:46 2014 taieb_t$
*/

#include "malloc.h"

t_block		search_block(t_block *last, size_t size, void *base)
{
  t_block	b;

  b = base;
  while (b && !(b->free && b->size >= size))
    {
      *last = b;
      b = b->next;
    }
  return (b);
}

int		valid_addr(void *p, void *base)
{
  if (base)
    {
      if (p > base && p < sbrk(0))
	return (p == (get_block(p))->ptr);
    }
  return (0);
}

void		*unlock_mutex(pthread_mutex_t	*mutex)
{
  pthread_mutex_unlock(mutex);
  return (NULL);
}
